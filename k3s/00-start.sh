# On every node:
sudo apt-get update && sudo apt-get upgrade

# On primary:
curl -sfL https://get.k3s.io | sh -s - server --cluster-init
# Once this is complete, you can get the token for getting other nodes to join with:
sudo cat /var/lib/rancher/k3s/server/node-token

# If you want a SECOND PRIMARY server (for HA), you can add them with this command. Note that it ends in "server" to indicate this will be a server node
curl -sfL https://get.k3s.io | K3S_URL=https://<master1_ip>:6443 K3S_TOKEN=<node_token> sh -s - server

# On each node, install k3s
curl -sfL https://get.k3s.io | sh -s -
# then join it to the cluster
sudo k3s agent --server https://<primary-ip>:6443 --token <join-token>
