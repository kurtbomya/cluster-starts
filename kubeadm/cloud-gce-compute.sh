# DO NOT RUN THIS FILE DIRECTLY, ITS WRITTEN FOR A HUMAN TO COPY/PASTE LINES OUT OF. EXTENSION NAME IS FOR IDE SYNTAX HIGHLIGHTING TO INCREASE COMPREHNSION
# The cluster secrets in this file are for a cluster that doesn't exist. Don't sweat it.

# on the primary server... (This file only creates 1 primary server)

# Disable swap
sudo swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-k8s.conf
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

sudo sysctl --system

# Install containerd runtime, we don't want to use docker as K8s project abandoned docker
sudo apt update
sudo apt -y install containerd

mkdir -p /etc/containerd/

containerd config default | sudo tee /etc/containerd/config.toml >/dev/null 2>&1

# Edit the file ‘/etc/containerd/config.toml’
sudo vi /etc/containerd/config.toml
# and look for the section
# ‘[plugins.”io.containerd.grpc.v1.cri”.containerd.runtimes.runc.options]’
# add "SystemdCgroup = true" under this section

sudo systemctl restart containerd
sudo systemctl enable containerd

sudo apt install gnupg gnupg2 curl software-properties-common -y
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /" | sudo tee /etc/apt/sources.list.d/kubernetes.list
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg

sudo apt update
sudo apt install kubelet kubeadm kubectl -y

# Prevent automation from changing your k8s version. If someone does an upgrade to a version that has different APIs (which happens often) and you haven't updated your manifests, you'll be down for 4 hours like reddit last month for the same reason
sudo apt-mark hold kubelet kubeadm kubectl

sudo kubeadm init --control-plane-endpoint=k8s-prime

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

kubectl get nodes
kubectl cluster-info

kubectl apply -f https://projectcalico.docs.tigera.io/manifests/calico.yaml

kubectl get pods -n kube-system
kubectl get nodes


#### SWITCHING TO EACH WORKER NODE NOW, do the following ###################
sudo swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-k8s.conf
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

sudo sysctl --system

sudo apt update
sudo apt -y install containerd

sudo mkdir -p /etc/containerd/

containerd config default | sudo tee /etc/containerd/config.toml >/dev/null 2>&1

# Edit the file ‘/etc/containerd/config.toml’
sudo vi /etc/containerd/config.toml
# and look for the section
# ‘[plugins.”io.containerd.grpc.v1.cri”.containerd.runtimes.runc.options]’ and add SystemdCgroup = true

sudo systemctl restart containerd
sudo systemctl enable containerd

sudo apt install gnupg gnupg2 curl software-properties-common -y
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /" | sudo tee /etc/apt/sources.list.d/kubernetes.list
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg

sudo apt update
sudo apt install kubelet kubeadm kubectl -y
sudo apt-mark hold kubelet kubeadm kubectl

# Don't use my values, use your own secret keys
kubeadm join k8s-prime:6443 --token kmww09.jhvvsafxewxzstbb \
      --discovery-token-ca-cert-hash sha256:995d05e07ca973ee4f86aa5fd9cd023e77d48e91fa7c1e8b272ff07b50f87b0e

### Troubleshooting a join
sudo kubeadm token create --print-join-command
sudo kubeadm join k8s-prime:6443 --token 6nnb2m.luvehcq714azk1yy --discovery-token-ca-cert-hash sha256:995d05e07ca973ee4f86aa5fd9cd023e77d48e91fa7c1e8b272ff07b50f87b0e
