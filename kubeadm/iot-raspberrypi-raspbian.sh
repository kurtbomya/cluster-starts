# For raspberry pi running raspbian, this is how you join it to kubeadm cluster

# add in all nodes
sudo vi /etc/hosts
# Edit this file.  # set CONF_SWAPSIZE=0
sudo vi /etc/dphys-swapfile
sudo reboot now
# Verify swap is 0
free -m

cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-k8s.conf
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

sudo sysctl --system

sudo apt-get update
sudo apt-get install ca-certificates curl gnupg gnupg2 software-properties-common -y
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
echo   "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" |   sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt install containerd.io
systemctl status containerd
# Verify it worked
containerd --version

containerd config default | sudo tee /etc/containerd/config.toml >/dev/null 2>&1
# Edit the file ‘/etc/containerd/config.toml’
sudo vi /etc/containerd/config.toml
# and look for the section
# [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
# add "SystemdCgroup = true" under this section

# Edit /boot/cmdline.txt
sudo vi /boot/cmdline.txt
# Add to the one liner, space delimited "cgroup_memory=1 cgroup_enable=memory"

# install kubeadm, kubectl, and kubelet
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/cgoogle.gpg
sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
sudo apt update
sudo apt install kubelet kubeadm kubectl -y
sudo apt-mark hold kubelet kubeadm kubectl

sudo systemctl start systemd-resolved
sudo systemctl enable systemd-resolved
sudo kubeadm join k8s-prime:6443 --token <token> --discovery-token-ca-cert-hash sha256:bacon

